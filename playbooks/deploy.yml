- name: "core"
  hosts: 
    - cluster
  become: true
  gather_facts: False
  tasks:
    - include_role:
        name: "core"
        allow_duplicates: no
        apply:
          tags:
            - core
      tags:
        - core
  tags:
    - core

- name: "hardening"
  hosts: 
    - cluster
  become: true
  gather_facts: False
  tasks:
    - include_role:
        name: "dev-sec.os-hardening"
        allow_duplicates: no
        apply:
          tags:
            - hardening
      vars:
        sysctl_overwrite:
          net.ipv4.ip_forward: 1
          net.ipv6.conf.all.forwarding: 1
          net.ipv4.tcp_window_scaling: 0
          net.ipv6.conf.all.disable_ipv6: 0
      tags:
        - hardening
  tags:
    - hardening

- name: "cluster"
  hosts: 
    - cluster
  become: true
  gather_facts: False
  strategy: linear
  tasks:
    - include_role:
        name: "cluster"
        allow_duplicates: no
        apply:
          tags:
            - cluster
      tags:
        - cluster
  tags:
    - cluster

- name: "engine"
  hosts: 
    - cluster
  serial: 1
  become: true
  gather_facts: False
  tasks:
    - include_role:
        name: "providers/{{ arc['engine']['provider'] }}"
        allow_duplicates: no
        apply:
          tags:
            - "{{ arc['engine']['provider'] }}"
            - engine
      tags:
        - engine
        - "{{ arc['engine']['provider'] }}"
      when:
        - arc['engine']['enabled']|bool

- name: "orchestrator"
  hosts: 
    - cluster
  become: true
  tasks:
    - include_role:
        name: "providers/{{ arc['orchestrator']['provider'] }}"
        allow_duplicates: no
        apply:
          tags:
            - "{{ arc['orchestrator']['provider'] }}"
            - orchestrator
      tags:
        - orchestrator
        - "{{ arc['orchestrator']['provider'] }}"
      when:
        - arc['orchestrator']['enabled']|bool

- name: "cleanup"
  become: True
  gather_facts: False
  hosts: manager[0]
  tasks:
    - name: "Gather Package facts"
      package_facts:
        manager: "auto"

    - name: "removing legacy docker stacks"
      docker_stack:
        absent_retries: 5
        absent_retries_interval: 10
        state: absent
        name: "{{ item }}"
      when: 
        - "'docker-ce' in ansible_facts.packages"
        - arc['orchestrator']['provider'] == "swarm"
      with_items:
        - cadvisor
        - alertmanager
        - prometheus
        - node-exporter
        - promtail
        - grafana
        - garbage-collector
        - loki

- name: "management"
  hosts: 
    - manager[0]
  become: true
  strategy: linear
  gather_facts: False
  tasks:
    - include_role:
        name: "providers/{{ arc['management']['provider'] }}"
        allow_duplicates: no
        apply:
          tags:
            - "{{ arc['management']['provider'] }}"
            - management
      tags:
        - "{{ arc['management']['provider'] }}"
        - management

- name: "data"
  hosts: 
    - cluster
  become: true
  strategy: linear
  gather_facts: False
  tasks:
    - include_role:
        name: "providers/{{ arc['data']['provider'] }}"
        allow_duplicates: no
        apply:
          tags:
            - "{{ arc['data']['provider'] }}"
            - data
      tags:
        - "{{ arc['data']['provider'] }}"
        - data
      when:
        - arc['data']['provider'] != "generic"

- name: "collectors"
  hosts: 
    - cluster
  become: true
  gather_facts: False
  tasks:
    - include_role:
        name: "providers/node-exporter"
        allow_duplicates: no
        apply:
          tags:
            - metrics
            - node-exporter
      tags:
        - metrics
        - node-exporter

    - include_role:
        name: "providers/cadvisor"
        allow_duplicates: no
        apply:
          tags:
            - metrics
            - cadvisor
      tags:
        - metrics
        - node-exporter

    - include_role:
        name: "providers/process-exporter"
        allow_duplicates: no
      tags:
        - metrics
        - process-exporter

    - include_role:
        name: "providers/promtail"
        allow_duplicates: no
      tags:
        - metrics
        - promtail

- name: "metrics"
  hosts: 
    - manager[0]
  become: true
  gather_facts: False
  tasks:
    - include_role:
        name: "providers/{{ arc['metrics']['provider'] }}"
        allow_duplicates: no
        apply:
          tags:
            - "{{ arc['metrics']['provider'] }}"
            - metrics
      tags:
        - "{{ arc['metrics']['provider'] }}"
        - metrics

- name: "logs"
  hosts: 
    - manager[0]
  become: true
  gather_facts: False
  tasks:
    - include_role:
        name: "providers/{{ arc['logs']['provider'] }}"
        allow_duplicates: no
        apply:
          tags:
            - "{{ arc['logs']['provider'] }}"
            - logs
      tags:
        - "{{ arc['logs']['provider'] }}"
        - logs

- name: "alerts"
  hosts: 
    - manager[0]
  become: true
  gather_facts: False
  tasks:
    - include_role:
        name: "providers/{{ arc['alerts']['provider'] }}"
        allow_duplicates: no
        apply:
          tags:
            - "{{ arc['alerts']['provider'] }}"
            - alerts
      tags:
        - "{{ arc['alerts']['provider'] }}"
        - alerts

- name: "analytics"
  hosts: 
    - manager[0]
  become: true
  gather_facts: False
  tasks:
    - include_role:
        name: "providers/{{ arc['analytics']['provider'] }}"
        allow_duplicates: no
        apply:
          tags:
            - "{{ arc['analytics']['provider'] }}"
            - analytics
      tags:
        - "{{ arc['analytics']['provider'] }}"
        - analytics

# Todo
# Backups, Garbage Collection

- name: "addons"
  hosts: 
    - manager[0]
  become: true
  gather_facts: False
  tasks:
    # - debug:
    #     msg: "{{ item }}"
    #   with_items: "{{ arc['addons'] }}"
    #   tags:
    #     - addons
    # - debug:
    #     msg: "{{ arc['addons'] }}"
    #   tags:
    #     - addons
    - include_role:
        name: "addons/{{ addon }}"
        allow_duplicates: no
        apply:
          tags:
            - addons
            - "{{ addon }}"
      tags:
        - addons
      with_items: "{{ arc['addons'] }}"
      when: 
        - arc['addons'][addon]['enabled']|bool
      loop_control:
        loop_var: addon