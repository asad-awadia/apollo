---
- name: Provision promtail 
  block:
    - name: Creating app directory
      file:
        path: "{{ app_dir }}"
        state: directory
        mode: '0755'
        owner: "apollo"
        group: "apollo"

    - name: Create config directories
      file:
        path: "{{ app_dir }}/{{ config_dir.path }}"
        state: directory
        mode: '{{ config_dir.mode }}'
        owner: "apollo"
        group: "apollo"
      with_filetree:
        - "files/"
      when: config_dir.state == 'directory'
      loop_control:
        label: "{{ config_dir.path }}"
        loop_var: config_dir

    - name: Copy config files
      copy:
        src: "{{ config_file.src }}"
        dest: "{{ app_dir }}/{{ config_file.path }}"
        mode: "{{ config_file.mode }}"
        owner: "apollo"
        group: "apollo"
      with_filetree:
        - "files/"
      when: config_file.state == 'file'
      loop_control:
        label: "{{ config_file.path }}"
        loop_var: config_file

    - name: Copy promtail config
      template:
        src: templates/promtail-config.yml.j2
        dest: "{{ app_dir }}/promtail-config.yml"
        owner: "apollo"
        group: "apollo"
        mode: '0644'
      notify: restart promtail

    - name: Download promtail binary
      become: false
      get_url:
        url: "https://github.com/grafana/loki/releases/download/v{{ promtail_version }}/promtail-linux-amd64.zip"
        dest: "{{ apollo_downloads_dir }}/promtail-v{{ promtail_version }}.zip"
      register: _download_binary
      until: _download_binary is succeeded
      retries: 5
      delay: 2
      check_mode: false

    - name: Unpack promtail binary
      become: false
      unarchive:
        remote_src: True
        src: "{{ apollo_downloads_dir }}/promtail-v{{ promtail_version }}.zip"
        dest: "{{ apollo_downloads_dir }}"
        creates: "{{ apollo_downloads_dir }}/promtail-linux-amd64"
      check_mode: false

    - name: Propagate promtail binaries
      copy:
        src: "{{ apollo_downloads_dir }}/promtail-linux-amd64"
        dest: "{{ apollo_binary_dir }}/promtail"
        mode: 0755
        owner: apollo
        group: apollo
        remote_src: True
      notify: restart promtail
      when: not ansible_check_mode

    - name: Copy the promtail systemd service file
      template:
        src: templates/promtail.service.j2
        dest: /etc/systemd/system/promtail.service
        owner: root
        group: root
        mode: 0644
      notify: restart promtail

    - name: Make sure service is started
      service:
        name: promtail
        state: started
        enabled: true